(require 'org)
(require 'ox)


(setq org-publish-project-alist
      '(("org"
        :base-directory "/run/media/ffco/00FCF470FCF46178/Users/ffffconde/Desktop/WDG/page/"
        ; :base-directory "/mnt/c/Users/ffffconde/Desktop/WDG/page/"
        :base-extension "org"
        :publishing-directory "public/"
        :publishing-function org-html-publish-to-html
        :section-numbers nil
        :html-head "<link rel='stylesheet' href='css/custom.css' type='text/css'/>"
        :html-preamble
         "<header>
                <a href=\"/index.html\">about</a> ||
                <a href=\"posts/blog-index.html\">index</a> ||
         </header>"
        ;; :html-postamble
        :with-toc nil)

        ("posts"
         :base-directory "posts/"
         :base-extension "org"
         :publishing-directory "public/posts/"
         :recursive t
         :publishing-function org-html-publish-to-html
         :auto-sitemap t
         :sitemap-title "Blog Index"
         :sitemap-filename "blog-index.org"
         :sitemap-style list
         :author "pxoq"
         ;; this is not working on some files for some reason
         :html-head "<link rel='stylesheet' href='../css/custom.css' type='text/css'/>"
         :html-preamble
         "<header>
                <a href=\"../index.html\">about</a> ||
                <a href=\"blog-index.html\">index</a> ||
                <a href=\"../contact.html\">contact</a>
         </header>"
         ;; :html-postamble
         :email "john.doe@example.com"
         :with-creator t)
        ("images"
         :base-directory "images/"
         :base-extension "jpg\\|gif\\|png"
         :publishing-directory "public/images/"
         :publishing-function org-publish-attachment)
        ("js"
         :base-directory "js/"
         :base-extension "js"
         :publishing-directory "public/js/"
         :publishing-function org-publish-attachment)
        ("css"
         :base-directory "css/"
         :base-extension "css"
         :publishing-directory "public/css/"
         :publishing-function org-publish-attachment
         :recursive t)
;;        ("rss"
;;         :base-directory "/home/swbvty/code/websites/org-blog2/blog"
;;         :base-extension "org"
;;         :publishing-directory "~/public/org-blog2/blog"
;;         :publishing-function (org-rss-publish-to-rss)
;;         :html-link-home "~/public/org-blog2/"
;;         :html-link-use-abs-url t)
         ("website" :components ("org" "posts" "images" "js" "css"))))
