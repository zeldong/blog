#+TITLE: The Best Finance Books

A survey of results on the topic, with a emphasis on the most practical.  Note
of course, these are just the ones ive read, there are obviously other amazing
finance books but ive not read them.

the list is:

- 4 Pillars of Investng (Bernstein 2002)
- Ivy Portfolio (Faber 2009)
- Global Asset Allocation (Faber 2015)
- Simple Path to Wealth (Collin 2016)
- Simple Path to Wealth (Collin 2016)
- Index Funds: The 12-step Program for Active Investors (Hebner 2007)
- Guru Investor (Forehand, Reese 2009)
- The Misbehaviour of Markets (Mandelbrot 2004)
- Millionaire Next Door (Stanley 1996)
- Early Retirement Extreme (Fisker 2010)
- Tightwad Gazzete (Dacyczyn 1992)
- The Mathematics of Money Management (Vince 1992)

In regards to what is most important in understand and getting some benefits
from finance I would say Mandelbrot if you want to understand the general field
of finance and Faber if you want a good portfolio. All were valuable in my
understanding of it.
